
#include "eavlDataSet.h"
#include "eavlImporterFactory.h"
#include "eavlImporter.h"
#include "eavlArray.h"
#include "eavlCellSetExplicit.h"
#include "eavlCell.h"
#include "eavlFilter.h"
#include "eavlExternalFaceMutator.h"
#include "eavlExecutor.h"
#include "eavlMapOp.h"
#include "eavlOperation.h"
#include "eavlReduceOp_1.h"
#include "eavlGatherOp.h"
#include "eavlReverseIndexOp.h"
#include "eavlPrefixSumOp_1.h"
#include "eavlRadixSortOp.h"
#include "eavlInfoTopologyMapOp.h"
#include "eavlDestinationTopologyPackedMapOp.h"
#include "eavlTimer.h"
#include "eavlVTKExporter.h"
#include <algorithm>
#include <fstream>


  int main (int argc, char* argv[])
  {

        int totalTime = eavlTimer::Start();
        std::string filename(argv[1]);
        std::cout << "Input mesh file: " << filename << "\n";

        std::ofstream profiler;
        profiler.open("extface_eavl.prof");
        profiler << "Input mesh file: " << filename << "\n";
        profiler << "OpenMP Enabled: ";
        #ifdef HAVE_OPENMP
          profiler << "TRUE\n";
        #else
          profiler << "FALSE\n";
        #endif

        int timer  = eavlTimer::Start();
        eavlImporter *importer = eavlImporterFactory::GetImporterForFile(filename);


        //--------------------------Getting mesh info-----------------------------------------------
        std::vector<std::string> meshlist = importer->GetMeshList();
        int msize = meshlist.size();
        bool meshSpecified = false;
        int meshIdx = 0;
        int domainindex = 0;

        if ( (!meshSpecified && msize > 1) || ( meshSpecified || meshIdx > msize) )
        {
            std::cout << "\n" << "More than one mesh in the file. Please specify index of the mesh."
                      << "\n";
            for (int i = 0; i < msize; i++)
            {
                std::cout << "Index " << i << " " << meshlist.at(i) << "\n";
            }

            return 0;
        }
        profiler << "Import the mesh: " << eavlTimer::Stop(timer,"") << "\n";
        std::cout << "Using mesh: "<< meshlist.at(meshIdx) << "\n";
        eavlDataSet* data = importer->GetMesh(meshlist.at(meshIdx), domainindex);

        data->PrintSummary(std::cout);
	eavlExternalFaceMutator* mut = new eavlExternalFaceMutator();
	mut->SetDataSet(data);
	mut->SetCellSet(data->GetCellSet(0)->GetName());

        timer  = eavlTimer::Start();
	mut->Execute();
         profiler << "Total time: " << eavlTimer::Stop(totalTime,"") << std::endl;
         profiler.close();
	delete mut; 

	return 0;

    }
