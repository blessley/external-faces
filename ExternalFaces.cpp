//Causes the compiler to produce an error if any object is
//instantiated with the default device adapter
//#define VTKM_DEVICE_ADAPTER VTKM_DEVICE_ADAPTER_ERROR
//For CUDA: VTKM_DEVICE_ADAPTER_CUDA

//VTK-m header files (within vtkm/vtkm)
#include <vtkm/cont/DeviceAdapter.h>
#include <vtkm/cont/ArrayHandle.h>
#include <vtkm/cont/ArrayHandleCounting.h>
#include <vtkm/cont/ArrayHandleConstant.h>
#include <vtkm/cont/ArrayHandlePermutation.h>
#include <vtkm/cont/Timer.h>
#include <vtkm/cont/DataSet.h>
#include <vtkm/CellType.h>
#include <vtkm/worklet/DispatcherMapField.h>
#include <vtkm/worklet/DispatcherMapTopology.h>
#include <vtkm/worklet/WorkletMapField.h>
#include <vtkm/worklet/WorkletMapTopology.h>
#include <vtkm/exec/arg/TopologyIdSet.h>
#include <vtkm/exec/arg/TopologyIdCount.h>
#include <vtkm/exec/arg/TopologyElementType.h>

//EAVL header files (within vtkm/EAVL...assuming EAVL is built within vtkm)
/*
#include <EAVL/src/common/eavlDataSet.h>
#include <EAVL/src/importers/eavlImporterFactory.h>
#include <EAVL/src/importers/eavlImporter.h>
#include <EAVL/src/common/eavlArray.h>
#include <EAVL/src/common/eavlCellSetExplicit.h>
#include <EAVL/src/common/eavlCell.h>
*/

//std header files
#include <vector>

//Globally set the device adapter tag here
//For CUDA: vtkm::cont::DeviceAdapterTagCuda
typedef VTKM_DEFAULT_DEVICE_ADAPTER_TAG DeviceAdapter;

namespace worklets {

//Unary predicate operator
//Returns True if the argument is equal to 1; False otherwise.
struct IsUnity
{
    template<typename T>
    VTKM_EXEC_CONT_EXPORT bool operator()(const T &x) const
    {
        return x == 1;
    }
};

//Binary operator
//Returns (a -b) mod c
class Subtract : public vtkm::worklet::WorkletMapField
{
    private:
        vtkm::Id modulus;

    public:
        typedef void ControlSignature(FieldIn<>, FieldIn<>, FieldOut<>);
        typedef _3 ExecutionSignature(_1, _2);

        VTKM_CONT_EXPORT
        Subtract(const vtkm::Id &c) : modulus(c) { };

        template<typename T>
        VTKM_EXEC_CONT_EXPORT
        T operator()(const T &a, const T &b) const
        {
            return (a - b) % modulus;
        }
};


/*
static vtkm::Id getCellType(eavlCellShape type)
{
    if (type == EAVL_TET) return vtkm::VTKM_TETRA;
    else if (cellType == EAVL_PYRAMID) return vtkm::VTKM_PYRAMID;
    else if (cellType == EAVL_WEDGE) return vtkm::VTKM_WEDGE;
    else if (cellType == EAVL_HEX) return vtkm::VTKM_HEXAHEDRON;
    else return -1;
}
*/

//Worklet that returns the number of faces for each cell/shape
class NumFacesPerCell : public vtkm::worklet::WorkletMapField
{
    public:
        typedef void ControlSignature(FieldIn<>, FieldOut<>);
        typedef _2 ExecutionSignature(_1);
        typedef _1 InputDomain;

        VTKM_CONT_EXPORT
        NumFacesPerCell() { };

        template<typename T>
        VTKM_EXEC_EXPORT
        T operator()(const T &cellType) const
        {
            if (cellType == vtkm::VTKM_TETRA) return 4;
            else if (cellType == vtkm::VTKM_PYRAMID) return 5;
            else if (cellType == vtkm::VTKM_WEDGE) return 5;
            else if (cellType == vtkm::VTKM_HEXAHEDRON) return 6;
            else return -1;
        }
};


//Worklet that returns a hash key for a face
class FaceHashKey : public vtkm::worklet::WorkletMapTopology
{
    static const int LEN_IDS = 4; //The max num of nodes in a cell

    public:
        typedef void ControlSignature(//FieldSrcIn<Scalar> inNodes,
                                      FieldDestIn<AllTypes> localFaceIds,
                                      TopologyIn<LEN_IDS> topology,
                                      FieldDestOut<AllTypes> faceHashes
                                      );
        typedef void ExecutionSignature(//_1, _2, _4, _5, _6,
                                        _1, _3,
                                        vtkm::exec::arg::TopologyIdCount,
                                        vtkm::exec::arg::TopologyElementType,
                                        vtkm::exec::arg::TopologyIdSet);
        //typedef _3 InputDomain;
        typedef _2 InputDomain;

        VTKM_CONT_EXPORT
        FaceHashKey() { };

        template<typename T>
        VTKM_EXEC_EXPORT
        void operator()(//const vtkm::exec::TopologyData<T,LEN_IDS> & vtkmNotUsed(nodevals),
                        const T &cellFaceId,
                        T &faceHash,
                        const vtkm::Id & vtkmNotUsed(numNodes),
                        const vtkm::Id &cellType,
                        const vtkm::exec::TopologyData<vtkm::Id,LEN_IDS> &cellNodeIds) const
        {
            if (cellType == vtkm::VTKM_TETRA)
            {
                //Assign cell points/nodes to this face
                vtkm::Id faceP1, faceP2, faceP3;
                if(cellFaceId == 0)
                {
                    //Face A: (0, 1, 2)
                    faceP1 = cellNodeIds[0];
                    faceP2 = cellNodeIds[1];
                    faceP3 = cellNodeIds[2];
                }
                else if (cellFaceId == 1)
                {
                    //Face B: (0, 1, 3)
                    faceP1 = cellNodeIds[0];
                    faceP2 = cellNodeIds[1];
                    faceP3 = cellNodeIds[3];
                }
                else if (cellFaceId == 2)
                {
                    //Face C: (0, 2, 3)
                    faceP1 = cellNodeIds[0];
                    faceP2 = cellNodeIds[2];
                    faceP3 = cellNodeIds[3];
                }
                else if (cellFaceId == 3)
                {
                    //Face D: (1, 2, 3)
                    faceP1 = cellNodeIds[1];
                    faceP2 = cellNodeIds[2];
                    faceP3 = cellNodeIds[3];
                }

                //Sort the face points/nodes in ascending order
                vtkm::Id sorted[3] = {faceP1, faceP2, faceP3};
                vtkm::Id temp;
                if (sorted[0] > sorted[2])
                {
                    temp = sorted[0];
                    sorted[0] = sorted[2];
                    sorted[2] = temp;
                }
                if (sorted[0] > sorted[1])
                {
                    temp = sorted[0];
                    sorted[0] = sorted[1];
                    sorted[1] = temp;
                }
                if (sorted[1] > sorted[2])
                {
                    temp = sorted[1];
                    sorted[1] = sorted[2];
                    sorted[2] = temp;
                }

                //Calculate a hash key for the sorted points of this face
                unsigned int h  = 2166136261;
                for(int i = 0; i < 3; i++)
                    h = (h * 16777619) ^ sorted[i];

                faceHash = h;
            }
        }
};


} //worklets namespace

int main(int argc, char* argv[])
{

    //--------------Construct a VTK-m Dataset----------------

    vtkm::cont::DataSet ds;

    const int nVerts = 8; //A cube that is tetrahedralized
    vtkm::Float32 xVals[nVerts] = {0, 1, 1, 0, 0, 1, 1, 0};
    vtkm::Float32 yVals[nVerts] = {0, 0, 1, 1, 0, 0, 1, 1};
    vtkm::Float32 zVals[nVerts] = {0, 0, 0, 0, 1, 1, 1, 1};
    ds.AddField(vtkm::cont::Field("x", 1, vtkm::cont::Field::ASSOC_POINTS, xVals, nVerts));
    ds.AddField(vtkm::cont::Field("y", 1, vtkm::cont::Field::ASSOC_POINTS, yVals, nVerts));
    ds.AddField(vtkm::cont::Field("z", 1, vtkm::cont::Field::ASSOC_POINTS, zVals, nVerts));
    ds.AddCoordinateSystem(vtkm::cont::CoordinateSystem("x","y","z"));

    //Construct the VTK-m shapes and numIndices connectiviy arrays
    const int nCells = 6;  //The tetrahedrons of the cube
    int cellVerts[nCells][4] = {{4,7,6,3}, {4,6,3,2}, {4,0,3,2},
                               {4,6,5,2}, {4,5,0,2}, {1,0,5,2}};
    boost::shared_ptr< vtkm::cont::CellSetExplicit<> > cs(
                                new vtkm::cont::CellSetExplicit<>("cells", nCells));
    vtkm::cont::ArrayHandle<vtkm::Id> shapes;
    vtkm::cont::ArrayHandle<vtkm::Id> numIndices;
    shapes.Allocate(static_cast<vtkm::Id>(nCells));
    numIndices.Allocate(static_cast<vtkm::Id>(nCells));

    int index = 0;
    for(int j = 0; j < nCells; j++)
    {
        shapes.GetPortalControl().Set(j, static_cast<vtkm::Id>(vtkm::VTKM_TETRA));
        numIndices.GetPortalControl().Set(j, 4);
    }

    //---------Beginning VTK-m External Faces work----------------------------

    //Create a worklet to map the number of faces to each cell
    vtkm::cont::ArrayHandle<vtkm::Id> facesPerCell;
    vtkm::worklet::DispatcherMapField<worklets::NumFacesPerCell> numFacesDispatcher;
    vtkm::cont::DeviceAdapterTimerImplementation<DeviceAdapter> timer;
    numFacesDispatcher.Invoke(shapes, facesPerCell);
    std::cout << "NumFacesWorklet," << timer.GetElapsedTime() << "\n";

    //Exclusive scan of the number of faces per cell
    vtkm::cont::ArrayHandle<vtkm::Id> numFacesPrefixSum;
    timer.Reset();
    const vtkm::Id totalFaces =
    vtkm::cont::DeviceAdapterAlgorithm<DeviceAdapter>::ScanInclusive(facesPerCell,
                                                                     numFacesPrefixSum);
    std::cout << "ScanExclusive," << timer.GetElapsedTime() << "\n";
    if(totalFaces == 0) return 0;

    //Generate reverse lookup: face index to cell index
    //terminate if no cells have triangles left
    //For 2 tetrahedron cells with 4 faces each (array of size 8): 0,0,0,0,1,1,1,1
    vtkm::cont::ArrayHandle<vtkm::Id> face2CellId;
    vtkm::cont::ArrayHandle<vtkm::Id> localFaceIds;
    localFaceIds.Allocate(static_cast<vtkm::Id>(totalFaces));
    vtkm::cont::ArrayHandleCounting<vtkm::Id> countingArray(vtkm::Id(0), vtkm::Id(totalFaces));
    timer.Reset();
    vtkm::cont::DeviceAdapterAlgorithm<DeviceAdapter>::UpperBounds(numFacesPrefixSum,
                                                                 countingArray,
                                                                 face2CellId);
    std::cout << "UpperBounds," << timer.GetElapsedTime() << "\n";

    timer.Reset();
    vtkm::worklet::DispatcherMapField<worklets::Subtract> subtractDispatcher(worklets::Subtract(4));
    subtractDispatcher.Invoke(countingArray, face2CellId, localFaceIds);
    std::cout << "SubtractWorklet," << timer.GetElapsedTime() << "\n";
    countingArray.ReleaseResources();

    //Construct a connectivity array of length 4*totalFaces (4 repeat entries for each tet)
    vtkm::cont::ArrayHandle<vtkm::Id> faceConn;
    faceConn.Allocate(static_cast<vtkm::Id>(4 * totalFaces));
    index = 0;
    for(int i = 0; i < nCells; i++)
        for(int j = 0; j < facesPerCell.GetPortalConstControl().Get(i); j++)
            for(int k = 0; k < 4; k++)
                faceConn.GetPortalControl().Set(index++, static_cast<vtkm::Id>(cellVerts[i][k]));

    //Calculate a hash key for each cell face
    typedef vtkm::cont::ArrayHandle<vtkm::Id> IdHandleType;
    typedef vtkm::cont::ArrayHandlePermutation<IdHandleType, IdHandleType> IdPermutationType;
    typedef vtkm::cont::ExplicitConnectivity<IdPermutationType::StorageTag,
                                             IdPermutationType::StorageTag,
                                             IdHandleType::StorageTag> PermutedExplicitConnectivity;
    IdPermutationType pt1(face2CellId, shapes);
    IdPermutationType pt2(face2CellId, numIndices);
    PermutedExplicitConnectivity permConn;
    permConn.Fill(pt1, pt2, faceConn);
    vtkm::cont::ArrayHandle<vtkm::Id> faceHashes;
    vtkm::worklet::DispatcherMapTopology<worklets::FaceHashKey> faceHashDispatcher;
    timer.Reset();
    faceHashDispatcher.Invoke(localFaceIds, permConn, faceHashes);
    std::cout << "FaceHashKeyWorklet," << timer.GetElapsedTime() << "\n";
    face2CellId.ReleaseResources();
    localFaceIds.ReleaseResources();

    //Sort the faces in ascending order by hash key
    timer.Reset();
    vtkm::cont::DeviceAdapterAlgorithm<DeviceAdapter>::Sort(faceHashes);
    std::cout << "Sort," << timer.GetElapsedTime() << "\n";

    //Search neighboring faces/hashes for duplicates - the internal faces
    vtkm::cont::ArrayHandle<vtkm::Id> uniqueFaceHashes;
    vtkm::cont::ArrayHandle<vtkm::Id> uniqueHashCounts;
    vtkm::cont::ArrayHandle<vtkm::Id> externalFaceHashes;
    vtkm::cont::ArrayHandleConstant<vtkm::Id> ones(1, totalFaces); //Initially all 1's
    timer.Reset();
    vtkm::cont::DeviceAdapterAlgorithm<DeviceAdapter>::ReduceByKey(faceHashes,
                                                                   ones,
                                                                   uniqueFaceHashes,
                                                                   uniqueHashCounts,
                                                                   vtkm::internal::Add());
    std::cout << "ReduceByKey," << timer.GetElapsedTime() << "\n";
    faceHashes.ReleaseResources();
    ones.ReleaseResources();

    //Removes all faces/keys that have counts not equal to 1 (unity)
    //The faces with counts of 1 are the external faces
    timer.Reset();
    vtkm::cont::DeviceAdapterAlgorithm<DeviceAdapter>::StreamCompact(uniqueFaceHashes,
                                                                     uniqueHashCounts,
                                                                     externalFaceHashes,
                                                                     worklets::IsUnity());
    std::cout << "StreamCompact," << timer.GetElapsedTime() << "\n";
    uniqueFaceHashes.ReleaseResources();
    uniqueHashCounts.ReleaseResources();

    //The number of external faces
    vtkm::Id numExtFaces = externalFaceHashes.GetNumberOfValues();
    std::cout << "Total External Faces = " << numExtFaces << std::endl;
    externalFaceHashes.ReleaseResources();

    return 0;

}
