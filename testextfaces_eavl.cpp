
#include "eavlDataSet.h"
#include "eavlImporterFactory.h"
#include "eavlImporter.h"
#include "eavlArray.h"
#include "eavlCellSetExplicit.h"
#include "eavlCell.h"
#include "eavlExecutor.h"
#include "eavlMapOp.h"
#include "eavlOperation.h"
#include "eavlReduceOp_1.h"
#include "eavlGatherOp.h"
#include "eavlReverseIndexOp.h"
#include "eavlPrefixSumOp_1.h"
#include "eavlRadixSortOp.h"
#include "eavlInfoTopologyMapOp.h"
#include "eavlDestinationTopologyPackedMapOp.h"
#include "eavlTimer.h"
#include <algorithm>
#include <fstream>

struct NumFacesPerCell
{
    EAVL_FUNCTOR int operator()(int cellType)
    {
        if (cellType == EAVL_TET) return 4;
        else if (cellType == EAVL_PYRAMID) return 5;
        else if (cellType == EAVL_WEDGE) return 5;
        else if (cellType == EAVL_HEX) return 6;
        else return -1;
    }
};


struct ComputeHash
{   
    EAVL_FUNCTOR unsigned int  operator()(tuple<int, int, int>input)
    {
	unsigned int inputs[3] = {get<0>(input), get<1>(input), get<2>(input)};
        unsigned int h  = 2166136261;
        for(int i = 0; i < 3; i++)
        {
          h = (h * 16777619) ^ inputs[i];
        }
        return h;
    }
   
	/*
    EAVL_FUNCTOR int operator()(tuple<int, int, int>input)
    {
        int inputs[3] = {get<0>(input), get<1>(input), get<2>(input)};
        int h  = 21661362617;
        for(int i = 0; i < 3; i++)
        {
          h = (h * 1677761) ^ inputs[i];
        }
        return h;
    }
	*/

};

struct GetCellType
{
    EAVL_FUNCTOR int operator()(int cellType, int i1)
    {
        return cellType;
    }
};

struct IsInternalFace
{
    EAVL_FUNCTOR int operator()(tuple<int, int>input)
    {
        if (get<0>(input) == get<1>(input)) return 1;
        else return 0;
    }
};

struct GetCellPoints
{
    EAVL_FUNCTOR tuple<int, int, int, int, int> operator()(int cellType, int n, int cellPointIds[],
                                                      int localFaceId)
    {
	      return tuple<int, int, int, int, int>(cellPointIds[0], cellPointIds[1],
                                              cellPointIds[2], cellPointIds[3],
                                              cellType);
    }
};


struct AscendingSort_3
{
    EAVL_FUNCTOR tuple<int, int, int> operator()(tuple<int, int, int>input)
    {
        int sorted[3] = {get<0>(input), get<1>(input), get<2>(input)};
        std::sort(sorted, sorted + 3);
	      return tuple<int, int, int>(sorted[0], sorted[1], sorted[2]);
    }
};

struct GetFacePoints
{
    EAVL_FUNCTOR tuple<int, int, int> operator()(tuple<int, int, int, int, int, int>input)
    {
	  int f1 = 0, f2 = 0, f3 = 0;
          if (get<5>(input) == EAVL_TET)
          {
              if(get<4>(input) == 0)
              {
                  //Face A: (0, 1, 2)
                  f1 = get<0>(input);
                  f2 = get<1>(input);
                  f3 = get<2>(input);
              }
              else if (get<4>(input) == 1)
              {
                  //Face B: (0, 1, 3)
                  f1 = get<0>(input);
                  f2 = get<1>(input);
                  f3 = get<3>(input);
              }
              else if (get<4>(input) == 2)
              {
                  //Face C: (0, 2, 3)
                  f1 = get<0>(input);
                  f2 = get<2>(input);
                  f3 = get<3>(input);
              }
              else if (get<4>(input) == 3)
              {
                  //Face D: (1, 2, 3)
                  f1 = get<1>(input);
                  f2 = get<2>(input);
                  f3 = get<3>(input);
              }
          }
	  return tuple<int, int, int>(f1, f2, f3);
    }
};


  int main (int argc, char* argv[])
  {

        int totalTime = eavlTimer::Start();
        std::string filename(argv[1]);
        std::cout << "Input mesh file: " << filename << "\n";

        std::ofstream profiler;
	std::ofstream csv
        profiler.open(argv[2]);
        //profiler << "Input mesh file: " << filename << "\n";
        //profiler << "OpenMP Enabled: ";
        //#ifdef HAVE_OPENMP
          //profiler << "TRUE\n";
        //#else
          //profiler << "FALSE\n";
        //#endif

        int timer  = eavlTimer::Start();
        eavlImporter *importer = eavlImporterFactory::GetImporterForFile(filename);


        //--------------------------Getting mesh info-----------------------------------------------
        std::vector<std::string> meshlist = importer->GetMeshList();
        int msize = meshlist.size();
        bool meshSpecified = false;
        int meshIdx = 0;
        int domainindex = 0;

        if ( (!meshSpecified && msize > 1) || ( meshSpecified || meshIdx > msize) )
        {
            std::cout << "\n" << "More than one mesh in the file. Please specify index of the mesh."
                      << "\n";
            for (int i = 0; i < msize; i++)
            {
                std::cout << "Index " << i << " " << meshlist.at(i) << "\n";
            }

            return 0;
        }
        profiler << "Import the mesh," << eavlTimer::Stop(timer,"") << "\n";
        std::cout << "Using mesh: "<< meshlist.at(meshIdx) << "\n";
        eavlDataSet* data = importer->GetMesh(meshlist.at(meshIdx), domainindex);

        data->PrintSummary(std::cout);

        timer  = eavlTimer::Start();
        eavlCellSetExplicit* cellSet = (eavlCellSetExplicit*) data->GetCellSet(0);

        int numCells = cellSet->GetNumCells();
        profiler << "Eavl Function Call," << eavlTimer::Stop(timer,"") << "\n";
        std::cout << "Number of Cells: " << numCells << "\n";

	eavlIntArray* cellTypes = new eavlIntArray("cellTypes", 1, numCells);
  	eavlIntArray* inputTemp = new eavlIntArray("inputTemp", 1, numCells);
  	eavlIntArray* tempSum = new eavlIntArray("tempSum", 1, 1);
  	timer  = eavlTimer::Start();
  	eavlExecutor::AddOperation(new_eavlInfoTopologyMapOp(cellSet,
                                        EAVL_NODES_OF_CELLS,
                                        eavlOpArgs(inputTemp),
                                        eavlOpArgs(cellTypes),
                                        GetCellType()),
                                        "Get the shape/type for each cell.");
  	eavlExecutor::Go();
  	profiler << "InfoTopologyMapOp," << eavlTimer::Stop(timer,"") << "\n";
	cellTypes->PrintSummary(std::cout);
  	delete inputTemp;

        eavlIntArray* numFacesPerCell = new eavlIntArray("numFacesPerCell", 1, numCells);
        eavlIntArray* numFacesPrefixSum = new eavlIntArray("numFacesPrefixSum", 1, numCells);
        timer  = eavlTimer::Start();
	eavlExecutor::AddOperation (new_eavlMapOp(eavlOpArgs(cellTypes),
                                                 eavlOpArgs(numFacesPerCell),
                                                 NumFacesPerCell()),
                                  "Get the number of faces per cell.");
        eavlExecutor::Go();
        profiler << "MapOp," << eavlTimer::Stop(timer,"") << "\n";
        numFacesPerCell->PrintSummary(std::cout);

        timer  = eavlTimer::Start();
	eavlExecutor::AddOperation(new eavlPrefixSumOp_1(numFacesPerCell,
                                                         numFacesPrefixSum,
                                                         false),
                                                         "Exclusive scan of number of faces per cell");
        eavlExecutor::Go();
        profiler << "PrefixSumOp," << eavlTimer::Stop(timer,"") << "\n";
	numFacesPrefixSum->PrintSummary(std::cout);

        timer  = eavlTimer::Start();
        eavlExecutor::AddOperation(new eavlReduceOp_1<eavlAddFunctor<int> >
                                      (numFacesPerCell, tempSum, eavlAddFunctor<int>()),
                                      "Compute the total number of faces.");
        eavlExecutor::Go();
        unsigned int numFaces = tempSum->GetValue(0);
        profiler << "ReduceOp," << eavlTimer::Stop(timer,"") << "\n";
	tempSum->PrintSummary(std::cout);
	std::cout << "numFaces: " << numFaces << "\n";

        eavlIntArray* face2CellId = new eavlIntArray("face2CellId", 1, numFaces);
        eavlIntArray* localFaceIds = new eavlIntArray("localFaceIds", 1, numFaces);
	int maxOutValsPerInputCell = 4;
	timer  = eavlTimer::Start();
        eavlExecutor::AddOperation(new eavlReverseIndexOp(numFacesPerCell,
                                                                numFacesPrefixSum,
                                                                face2CellId,
								                                                       localFaceIds,
								maxOutValsPerInputCell),
                                                                "Generate reverse lookup: face index to cell index");
        eavlExecutor::Go();
        profiler << "ReverseIndexOp," << eavlTimer::Stop(timer,"") << "\n";
        localFaceIds->PrintSummary(std::cout);
        delete numFacesPrefixSum;
	face2CellId->PrintSummary(std::cout);

        eavlIntArray* faceIds = new eavlIntArray("faceIds", 1, numFaces);
        eavlIntArray* cellTypeIds = new eavlIntArray("cellTypeIds", 1, numFaces);

        //Input arrays (length of the number of tetrahedron points)
        eavlIntArray* v1 = new eavlIntArray("v1", 1, numFaces);
        eavlIntArray* v2 = new eavlIntArray("v2", 1, numFaces);
        eavlIntArray* v3 = new eavlIntArray("v3", 1, numFaces);
        eavlIntArray* v4 = new eavlIntArray("v4", 1, numFaces);
	timer  = eavlTimer::Start();
        eavlExecutor::AddOperation(new_eavlDestinationTopologyPackedMapOp(cellSet,
                                              EAVL_NODES_OF_CELLS,
                                              eavlOpArgs(localFaceIds),
                                              eavlOpArgs(v1, v2, v3, v4, cellTypeIds),
                                              eavlOpArgs(face2CellId), //Index array of cell Ids
                                              GetCellPoints()),
                                              "Construct matrix of repeated cell points.");
        eavlExecutor::Go();
        profiler << "DestinationTopologyPackedMapOp," << eavlTimer::Stop(timer,"") << "\n";
        delete cellTypes;
        delete numFacesPerCell;
	v1->PrintSummary(std::cout);
	v2->PrintSummary(std::cout);
	v3->PrintSummary(std::cout);
	v4->PrintSummary(std::cout);

        //Output arrays (length of the number of points on a tetrahedron face)
        eavlIntArray* f1 = new eavlIntArray("f1", 1, numFaces);
        eavlIntArray* f2 = new eavlIntArray("f2", 1, numFaces);
        eavlIntArray* f3 = new eavlIntArray("f3", 1, numFaces);
	timer  = eavlTimer::Start();
        eavlExecutor::AddOperation(new_eavlMapOp(eavlOpArgs(v1, v2, v3, v4, localFaceIds, cellTypeIds),
                                                  eavlOpArgs(f1, f2, f3),
                                                  GetFacePoints()),
                                                  "Return the vertex index tuples of each cell face.");
        eavlExecutor::Go();
        profiler << "MapOp," << eavlTimer::Stop(timer,"") << "\n";
        delete cellTypeIds;
        delete v1;
        delete v2;
        delete v3;
        delete v4;
        delete face2CellId;
        delete localFaceIds;

	f1->PrintSummary(std::cout);
	f2->PrintSummary(std::cout);
	f3->PrintSummary(std::cout);

        eavlIntArray* s_f1 = new eavlIntArray("s_f1", 1, numFaces);
        eavlIntArray* s_f2 = new eavlIntArray("s_f2", 1, numFaces);
        eavlIntArray* s_f3 = new eavlIntArray("s_f3", 1, numFaces);
	timer  = eavlTimer::Start();
        eavlExecutor::AddOperation (new_eavlMapOp(eavlOpArgs(f1, f2, f3),
                                                  eavlOpArgs(s_f1, s_f2, s_f3),
                                                  AscendingSort_3()),
                                                  "Sort the vertex indices within each tuple.");
        eavlExecutor::Go();
        profiler << "MapOp," << eavlTimer::Stop(timer,"") << "\n";
        delete f1;
        delete f2;
        delete f3;
	s_f1->PrintSummary(std::cout);
	s_f2->PrintSummary(std::cout);
	s_f3->PrintSummary(std::cout);

        eavlIntArray* hashes = new eavlIntArray("hashes", 1, numFaces);
        timer  = eavlTimer::Start();
	eavlExecutor::AddOperation(new_eavlMapOp(eavlOpArgs(s_f1, s_f2, s_f3),
                                                  eavlOpArgs(hashes),
                                                  ComputeHash()),
                                                  "Calculate a hash key for the sorted vertex tuple of each face");
        eavlExecutor::Go();
        profiler << "MapOp," << eavlTimer::Stop(timer,"") << "\n";
        delete s_f1;
        delete s_f2;
        delete s_f3;

        eavlIntArray* values = new eavlIntArray("values", 1, numFaces);
        timer  = eavlTimer::Start();
	eavlExecutor::AddOperation(new_eavlRadixSortOp(eavlOpArgs(hashes),
                                                       eavlOpArgs(values), false),
                                                       "Sort the face hash values");

        eavlExecutor::Go();
        profiler << "RadixSortOp," << eavlTimer::Stop(timer,"") << "\n";
        delete values;

        eavlIntArray* isDup = new eavlIntArray("isDup", 1, numFaces);
        eavlIntArray* zeros = new eavlIntArray("zeros", 1, numFaces);
        zeros->SetValue(0, 1);
        eavlIntArray* ones = new eavlIntArray("ones", 1, numFaces);
        timer  = eavlTimer::Start();
	eavlExecutor::AddOperation(new eavlPrefixSumOp_1(zeros,
                                                         ones,
                                                        true),
                                                        "Inclusive scan to produce an array of all 1's.");	
        eavlExecutor::Go();
        profiler << "PrefixSum," << eavlTimer::Stop(timer,"") << "\n";
        eavlIntArray* onesEScan = new eavlIntArray("onesEScan", 1, numFaces - 1);
	timer  = eavlTimer::Start();
	eavlExecutor::AddOperation(new eavlPrefixSumOp_1(ones,
                                                         onesEScan,
                                                        false),
                                                        "Exclusive scan of the 1's array.");	
        eavlExecutor::Go();
	profiler << "PrefixSum," << eavlTimer::Stop(timer,"") << "\n";
        eavlIntArray* onesIScan = new eavlIntArray("onesIScan", 1, numFaces - 1);
        timer  = eavlTimer::Start();
	eavlExecutor::AddOperation(new eavlPrefixSumOp_1(ones,
                                                        onesIScan,
                                                        true),
                                                        "Inclusive scan of the 1's array.");
	eavlExecutor::Go();	
	profiler << "PrefixSum," << eavlTimer::Stop(timer,"") << "\n";
        eavlIntArray* hashesRShift = new eavlIntArray("hashesRShift", 1, numFaces - 1);
        timer = eavlTimer::Start();
	eavlExecutor::AddOperation(new_eavlGatherOp(eavlOpArgs(hashes),
                                                    eavlOpArgs(hashesRShift),
                                                    eavlOpArgs(onesIScan)),
                                                    "Gather an array of the keys, right-shifted by 1.");
        eavlExecutor::Go();
	profiler << "GatherOp," << eavlTimer::Stop(timer,"") << "\n";
	eavlIntArray* hashesLShift = new eavlIntArray("hashesLShift", 1, numFaces - 1);
	timer = eavlTimer::Start();        
	eavlExecutor::AddOperation(new_eavlGatherOp(eavlOpArgs(hashes),
                                                    eavlOpArgs(hashesLShift),
                                                    eavlOpArgs(onesEScan)),
                                                    "Gather an array of the keys, left-shifted by 1.");
	eavlExecutor::Go();        
	profiler << "GatherOp," << eavlTimer::Stop(timer,"") << "\n";
	timer = eavlTimer::Start();        
	eavlExecutor::AddOperation(new_eavlMapOp(eavlOpArgs(hashesLShift, hashesRShift),
                                                  eavlOpArgs(isDup),
                                                  IsInternalFace()),
                                                  "Search neighbors for duplicate faces");
        eavlExecutor::Go();
        profiler << "MapOp," << eavlTimer::Stop(timer,"") << "\n";
        zeros->PrintSummary(std::cout);
        ones->PrintSummary(std::cout);
        onesEScan->PrintSummary(std::cout);
        onesIScan->PrintSummary(std::cout);
        hashes->PrintSummary(std::cout);
        hashesRShift->PrintSummary(std::cout);
        hashesLShift->PrintSummary(std::cout);
        isDup->PrintSummary(std::cout);
	/*
	std::cout << isDup->GetValue(0);
	for(int i = 1; i < numFaces; i++)
		std::cout << " " << isDup->GetValue(i);
	std::cout << "\n";
	*/
        timer  = eavlTimer::Start();
        eavlExecutor::AddOperation(new eavlReduceOp_1<eavlAddFunctor<int> >
                                          (isDup, tempSum, eavlAddFunctor<int>()),
                                          "Count the number of internal/duplicate faces.");
        eavlExecutor::Go();
        profiler << "ReduceOp," << eavlTimer::Stop(timer,"") << "\n";
        unsigned int internalFaces = tempSum->GetValue(0);
        delete isDup;
        delete zeros;
        delete ones;
        delete onesEScan;
        delete onesIScan;
        delete hashesLShift;
        delete hashesRShift;
        delete hashes;
        delete tempSum;
	unsigned int temp = internalFaces << 1;
	std::cout << "temp: " << temp << "\n";
        unsigned int  externalFaces = numFaces - temp;
	std::cout << "Total Faces: " << numFaces << "\n";
        std::cout << "Number of Internal Faces: " << internalFaces << "\n";
       	std::cout << "Number of External Faces: " << externalFaces << std::endl;
         profiler << "Total time: " << eavlTimer::Stop(totalTime,"") << std::endl;
         profiler.close();

	 return 0;

    }
